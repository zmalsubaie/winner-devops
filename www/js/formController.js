var formController = function ($scope, $state, $timeout, sharedDataService) {
    $scope.name = '';
    $scope.age = '';
    $scope.mobile = '';
    $scope.email = '';
    $scope.errorText = '';
    $scope.successText = '';
    var mobPattern = /^([0-9+-]{8,15})/;
    var emailPattern = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    var showErrorText = function (text) {
        $scope.errorText = text;
        $timeout(function () {
            $scope.errorText = '';
        }, 2000);
    };
    var showSuccessText = function (text) {
        $scope.successText = text;
        $timeout(function () {
            $scope.successText = '';
        }, 2000);
    };
    var formValidation = function () {
        var valid = '';
        if ($scope.name === '') {
            valid = 'Please Enter Name';
        } else if ($scope.age === '') {
            valid = 'Please Enter Age';
        } else if (isNaN($scope.age)) {
            valid = 'Please Enter Valid Age';
        } else if (parseInt($scope.age) < 1 || parseInt($scope.age) > 99) {
            valid = 'Please Enter Age Between 01 to 99';
        } else if ($scope.mobile === '') {
            valid = 'Please Enter Mobile Number';
        } else if (!mobPattern.test($scope.mobile)) {
            valid = 'Please Enter A valid Mobile Number';
        } else if ($scope.email === '') {
            valid = 'Please Enter Email';
        } else if (!emailPattern.test($scope.email)) {
            valid = 'Please Enter A Valid Email';
        }
        return valid;
    };
    $scope.submit = function () {
        var result = formValidation();
        if (result !== '') {
            showErrorText(result);
        } else {
            showSuccessText('Form Submitted');
            var newUser = {'name': $scope.name, 'age': $scope.age, 'mobile': $scope.mobile, 'email': $scope.email};
            var dataArray = sharedDataService.getDataArray();
            dataArray.push(newUser);
            sharedDataService.saveDataArray(dataArray);
            $scope.clear();
        }
    };
    $scope.clear = function () {
        $scope.name = '';
        $scope.age = '';
        $scope.mobile = '';
        $scope.email = '';
    };
    $scope.back = function(){
        $state.go('login');
    };
};
dashBoardApp.controller('formController', formController);
formController.$inject = ['$scope', '$state', '$timeout','sharedDataService'];