var dashBoardController = function($scope,$state,$timeout,$rootScope){
    var loggedUser = $rootScope.firstName;
    if(localStorage.getItem('profileImage')){
        $scope.enableRemoveBtn = true;
        document.getElementById("img").src = 'data:image/jpeg;base64,'+localStorage.getItem('profileImage');
    }else{
        $scope.enableRemoveBtn = false;
    }
    $scope.openCamera = function(){
        var onCameraSuccess = function(imageURI){
            localStorage.setItem('profileImage',imageURI);
            document.getElementById("img").src = 'data:image/jpeg;base64,'+imageURI;
            $timeout(function(){
               $scope.enableRemoveBtn = true;
            });
        };
        var onCameraFail = function(message){
            console.log('failed to get Image from Camera');
         };
        navigator.camera.getPicture(onCameraSuccess, onCameraFail, {
                quality: 50,
                sourceType: Camera.PictureSourceType.CAMERA,
                destinationType: Camera.DestinationType.DATA_URL,
                allowEdit: true,
                correctOrientation: true,
                targetWidth: 1024,
                targetHeight: 768
        });
    };
    $scope.openGallery = function(){
        var onGallerySuccess = function(imageURI){
            localStorage.setItem('profileImage',imageURI);
            document.getElementById("img").src = 'data:image/jpeg;base64,'+imageURI;
            $timeout(function(){
               $scope.enableRemoveBtn = true;
            });
        };
        var onGalleryFail = function(message){
            console.log('failed to get Image from Camera');
         };
        navigator.camera.getPicture(onGallerySuccess, onGalleryFail, {
                quality: 50,
                sourceType: Camera.PictureSourceType.SAVEDPHOTOALBUM,
                destinationType: Camera.DestinationType.DATA_URL,
                allowEdit: true,
                correctOrientation: true,
                targetWidth: 1024,
                targetHeight: 768
        });
    };
    $scope.removeImage = function(){
        localStorage.removeItem('profileImage'); 
        document.getElementById("img").src = 'img/noavatar.png';
        $timeout(function(){
               $scope.enableRemoveBtn = false;
        });
    };
    $scope.logout = function(){
        $state.go('login');
    };
};
dashBoardApp.controller('dashBoardController',dashBoardController);
dashBoardController.$inject = ['$scope','$state','$timeout','$rootScope'];