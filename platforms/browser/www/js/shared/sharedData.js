(function () {
    'use strict';
    var sharedDataService = function ($state) {
        var dataArray = [
            { 'name': 'Alaa A. Abdelaal', 'age': '40', 'mobile': '+966507408042', 'email': 'Aabdelaal.c@stc.com.sa' },
            { 'name': 'Alio', 'age': '32', 'mobile': '966528428', 'email': 'alio@kayesonline.com' },
            { 'name': 'Meshaal N. AlOtaibi', 'age': '30', 'mobile': '966500036661', 'email': 'menalotaibi.c@stc.com.sa' },
            { 'name': 'Abdulrahman Mohammed Moslem', 'age': '24', 'mobile': '966541125230', 'email': 'amoslem.c@stc.com.sa' },
            { 'name': 'Abdulrahman Saud Aldawish', 'age': '30', 'mobile': '966533450815', 'email': 'aaldawish.c@stc.com.sa' },
            { 'name': 'Tariq Obaid AlHarbi', 'age': '31', 'mobile': '966595308885', 'email': 'toalharbi.c@stc.com.sa' },
            { 'name': 'Mohammed AlSaif', 'age': '31', 'mobile': '966555073783', 'email': 'msalsaif.c@stc.com.sa' },
            { 'name': 'Zayed Mohammed Alsubai', 'age': '29', 'mobile': '966559697079', 'email': 'zmalsubai.c@stc.com.sa' },
            { 'name': 'Santhosh Veeraraghavan', 'age': '30', 'mobile': '966540824494', 'email': 'sveeraraghavan.c@stc.com.sa' }
        ];
        return {
            saveDataArray: function (dataArray) {
                this.dataArray = dataArray;
            },
            getDataArray: function () {
                return dataArray;
            }
        }
    };
    dashBoardApp.service('sharedDataService', sharedDataService);
    sharedDataService.$inject = ['$state'];
})();