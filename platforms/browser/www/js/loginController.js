var loginController = function ($scope, $state, $timeout,$rootScope) {
    $scope.username = '';
    $scope.password = '';
    $scope.errorText = '';
    var showErrorText = function (text) {
        $scope.errorText = text;
        $timeout(function () {
            $scope.errorText = '';
        }, 4000);
    };
    var userArray = [
        { 'user': 'ABC', 'pWord': '123' },
        { 'user': 'DEF', 'pWord': '456' },
        { 'user': 'GHI', 'pWord': '789' }
    ];
    var checkLogin = function (username, password) {
        var login = false;
        angular.forEach(userArray, function (value, key) {
            if (value.user === username.toUpperCase() && value.pWord === password) {
                login = true;
            }
        });
        return login;
    };
    $scope.loginUser = function () {
        if ($scope.username === '' || $scope.password === '') {
            showErrorText('Please enter credentials');
        } else {
            if (checkLogin($scope.username, $scope.password)) {
                $rootScope.loggedUserName = $scope.username;
                $state.go('dashboard');
            } else {
                showErrorText('Wrong Username/Password');
            }
        }
    };
    $scope.signUp = function () {
        $state.go('form');
    };
    $scope.usersList = function () {
        $state.go('userlist');
    };
};

dashBoardApp.controller('loginController', loginController);
loginController.$inject = ['$scope', '$state', '$timeout','$rootScope'];