var userListController = function ($scope, $state, $timeout, $rootScope, sharedDataService) {
    $scope.dataArray = sharedDataService.getDataArray();
};
dashBoardApp.controller('userListController', userListController);
userListController.$inject = ['$scope', '$state', '$timeout', '$rootScope','sharedDataService'];