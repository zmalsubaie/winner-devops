var dashBoardApp = angular.module('dashBoardApp',['ui.router']);

var routerConfig = function($stateProvider,$urlRouterProvider){
    $urlRouterProvider.otherwise('/login');
    $stateProvider
    .state('login', {
        url : '/login',
        templateUrl: 'views/login.html',
        controller: 'loginController'
    })
    .state('dashboard', {
        url : '/dashboard',
        templateUrl: 'views/dashboard.html',
        controller: 'dashBoardController'
    })
    .state('form', {
        url : '/form',
        templateUrl: 'views/form.html',
        controller: 'formController'
    })
    .state('userlist', {
        url : '/userlist',
        templateUrl: 'views/userList.html',
        controller: 'userListController'
    })
};

routerConfig.$inject = ['$stateProvider','$urlRouterProvider'];
dashBoardApp.config(routerConfig);



